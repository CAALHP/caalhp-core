This is the CAALHPPrototype1 project.
To build and run the solution you will need the following:

1)	Visual Studio 2012 with service pack 3 installed (or the corresponding 
	MSBUILD	version for a build server).

2)	Visual Studio should be run as administrator as it needs to host web 
	service ports.
	
3)	NuGet Package Manager version => 2.7.4
	
4)	Windows Azure .NET SDK for VS 2012 : 
	http://www.windowsazure.com/en-us/downloads/
	
5)	If you want to run the CRIP_RFID_Mockup project, You will need the Phidgets
	drivers installed: http://www.phidgets.com/docs/Operating_System_Support