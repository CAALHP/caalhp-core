using System;
using System.Collections.Generic;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Events.Types;
using CAALHP.Library.Config;
using CAALHP.Library.Visitor;

namespace CAALHP.Library.State
{
    public class NotLoggedInState : IUserState
    {
        /// <summary>
        /// CAALHP is a context reference
        /// </summary>
        private readonly ICAALHP _caalhp;

        public NotLoggedInState(ICAALHP caalhp)
        {
            _caalhp = caalhp;
            Console.WriteLine("No user logged in");
        }

        public void UserLoggedIn(UserProfile user)
        {
            Console.WriteLine("Logging in user: " + user.FullName);
            _caalhp.UserState = new UserLoggedInState(_caalhp);
        }

        public void UserLoggedOut()
        {
            
        }

        public IList<PluginConfig> GetListOfInstalledApps()
        {
            var visitor = new GetListOfInstalledAppsNotLoggedInVisitor();
            _caalhp.Accept(visitor);
            return visitor.Result;
        }
    }
}