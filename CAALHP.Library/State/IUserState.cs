﻿using System.Collections.Generic;
using CAALHP.Events.Types;
using CAALHP.Library.Config;

namespace CAALHP.Library.State
{
    public interface IUserState
    {
        void UserLoggedIn(UserProfile user);
        void UserLoggedOut();
        IList<PluginConfig> GetListOfInstalledApps();
    }
}