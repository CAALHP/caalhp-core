﻿using System;
using System.Collections.Generic;
using CAALHP.Events.Types;
using CAALHP.Library.Config;
using CAALHP.Library.Visitor;

namespace CAALHP.Library.State
{
    public class UserLoggedInState : IUserState
    {
        private readonly ICAALHP _caalhp;

        public UserLoggedInState(ICAALHP caalhp)
        {
            _caalhp = caalhp;
            Console.WriteLine("user logged in");
        }

        public void UserLoggedIn(UserProfile user)
        {

        }

        public void UserLoggedOut()
        {
            _caalhp.UserState = new NotLoggedInState(_caalhp);
        }

        public IList<PluginConfig> GetListOfInstalledApps()
        {
            var visitor = new GetListOfInstalledAppsUserLoggedInVisitor();
            _caalhp.Accept(visitor);
            return visitor.Result;
        }
    }
}