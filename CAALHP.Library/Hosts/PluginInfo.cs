﻿using System;
using CAALHP.Contracts;

namespace CAALHP.Library.Hosts
{
    [Serializable]
    public class PluginInfo : IPluginInfo
    {
        public string Name { get; set; }
        public string LocationDir { get; set; }
        public int Index { get; set; }

        public PluginInfo(){}
    }
}