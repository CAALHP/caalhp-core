﻿using System.Collections.Generic;

namespace CAALHP.Library.Hosts
{
    public interface IServiceHost : IHost
    {
        void ActivateDrivers();
        IList<string> GetListOfEventTypes();
    }
}