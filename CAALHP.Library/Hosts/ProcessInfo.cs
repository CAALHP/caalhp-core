﻿using System.Globalization;
using System.Text;

namespace CAALHP.Library.Hosts
{
    public class ProcessInfo
    {
        public float PercentProcessorTime { get; set; }
        public long PrivateBytes { get; set; }
        public int ProcessId { get; set; }
        public string Name { get; set; }
        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.AppendLine("ProcessInfo");
            builder.AppendLine("Name : " + Name);
            builder.AppendLine("ProcessId : " + ProcessId);
            builder.AppendLine("% Processor Time : " + PercentProcessorTime);
            builder.AppendLine("Private Bytes : " + PrivateBytes.ToString("G", CultureInfo.InvariantCulture));
            return builder.ToString();
        }
    }
}
