﻿using CAALHP.Library.Visitor;

namespace CAALHP.Library.Hosts
{
    public interface IHostManager : IHostVisitable
    {
        IAppHost AppHost { get; }
        IDeviceDriverHost DeviceDriverHost { get; }
        IServiceHost ServiceHost { get; }
        //void ShowApp(string appName); 
        //void CloseApp(string appName);
        //IList<PluginConfig> GetListOfInstalledApps();
        //IList<PluginConfig> GetListOfInstalledDeviceDrivers();
        //void ActivateDeviceDrivers();
        //IList<PluginConfig> GetListOfRunningApps();
    }
}