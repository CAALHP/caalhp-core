﻿using System.Collections.Generic;
using CAALHP.Library.Config;

namespace CAALHP.Library.Hosts
{
    public interface IAppHost : IHost
    {
        //Dictionary<int, ProcessInfo> GetRunningAppsProcessInfo();
        IList<PluginConfig> GetListOfRunningApps();
        void ShowLocalApp(string appName);
        void CloseApp(int app);
        void SwitchToApp(int app);
        IList<PluginConfig> GetLocalInstalledApps();
        IList<PluginConfig> GetLocalRunningApps();
    }
}