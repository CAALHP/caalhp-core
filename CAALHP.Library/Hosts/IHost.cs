﻿using System.Collections.Generic;
using CAALHP.Library.Config;
using CAALHP.Library.Visitor;

namespace CAALHP.Library.Hosts
{
    public interface IHost : IHostVisitable
    {
        void ReportEvent(KeyValuePair<string, string> value);
        IList<PluginConfig> GetListOfInstalledApps();
        void ShowApp(string appName);
        void CloseApp(string appName);
        IList<PluginConfig> GetListOfInstalledDeviceDrivers();
        void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId);
        void SubscribeToEvents(string fullyQualifiedNameSpace, int processId);
        void ShutDown();
    }
}
