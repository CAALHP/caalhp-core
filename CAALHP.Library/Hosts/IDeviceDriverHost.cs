﻿using System.Collections.Generic;
using CAALHP.Library.Config;

namespace CAALHP.Library.Hosts
{
    public interface IDeviceDriverHost : IHost
    {
        void ActivateDrivers();
        //Dictionary<int, ProcessInfo> GetRunningDriversProcessInfo();
        List<string> GetListOfActiveDevices();
        void UpdateDriverList();
        IList<PluginConfig> GetLocalInstalledDeviceDrivers();
    }
}