﻿using System;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization.JSON;

namespace CAALHP.Library.Observer
{
    public class EventSubject : Subject
    {
        public System.Collections.Generic.KeyValuePair<string, string> SubjectState { get; set; }
        
        public event UserLoggedInEventHandler UserLoggedIn;

        protected override void OnUserLoggedIn()
        {
            var handler = UserLoggedIn;
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(SubjectState.Key);
            var obj = JsonSerializer.DeserializeEvent(SubjectState.Value, type);
            if (obj.GetType() != typeof (UserLoggedInEvent)) return;
            if (handler != null) 
                handler(this, new UserLoggedInEventArgs(obj as UserLoggedInEvent));
        }
        
        public event UserLoggedOutEventHandler UserLoggedOut;

        protected override void OnUserLoggedOut()
        {
            var handler = UserLoggedOut;
            //var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(SubjectState.Key);
            //dynamic obj = JsonSerializer.DeserializeEvent(SubjectState.Value, type);
            if (handler != null) handler(this, new UserLoggedOutEventArgs());
        }

        public event CloseCAALHPEventHandler CloseCAALHP;

        protected override void OnCloseCAALHP()
        {
            var handler = CloseCAALHP;
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(SubjectState.Key);
            var obj = JsonSerializer.DeserializeEvent(SubjectState.Value, type);
            if (obj.GetType() != typeof(GenericInfoEvent)) return;
            var genericInfoEvent = obj as GenericInfoEvent;
            if(genericInfoEvent != null && !genericInfoEvent.Info.Equals("CloseCAALHP")) return;
            if (handler != null)
                handler(this, new EventArgs());
        }
    }

}
