﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;

namespace CAALHP.Library.Observer
{
    /// <summary>
    /// The 'Subject' abstract class
    /// </summary>
    public abstract class Subject
    {
        /// <summary>
        /// dictionary with lists of observers. a "default" is added for unfiltered observers
        /// </summary>
        private readonly IDictionary<string, List<IEventObserver>> _observerDictionary =
            new Dictionary<string, List<IEventObserver>>()
            {
                //{ Namespaces.DefaultNamespace, new List<Observer>() },
                { EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(NewEventTypeAddedEvent)), new List<IEventObserver>() },
            };

        //private readonly IList<Observer> _observers = new List<Observer>();
        public IList<string> NameSpaces
        {
            get { return _observerDictionary.Keys.ToList(); }
        }

        /*public void Attach(Observer observer)
        {
            //_observerDictionary["default"].Add(observer);
            Attach(observer, Namespaces.DefaultNamespace);
        }*/

        /// <summary>
        /// Subcribe...
        /// </summary>
        /// <param name="observer"></param>
        /// <param name="fqns"></param>
        public void Attach(IEventObserver observer, string fqns)
        {
            if (!_observerDictionary.ContainsKey(fqns))
            {
                _observerDictionary.Add(fqns, new List<IEventObserver>());
                var newEvent = EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(NewEventTypeAddedEvent));
                Notify(newEvent, fqns);
            }
            _observerDictionary[fqns].Add(observer);
        }

        /// <summary>
        /// Special case notify
        /// </summary>
        /// <param name="fqns"></param>
        /// <param name="newFqns"></param>
        private void Notify(string fqns, string newFqns)
        {
            if (!_observerDictionary.ContainsKey(fqns)) return;
            for (var i = _observerDictionary[fqns].Count - 1; i >= 0; i--)
            {
                _observerDictionary[fqns][i].NewEventTypeUpdate(newFqns);
            }
        }

        /// <summary>
        /// Unsubscribe...
        /// </summary>
        /// <param name="observer"></param>
        public void Detach(IEventObserver observer)
        {
            //_observerDictionary["default"].RemoveAll(o => o.Id == id);
            for (var i = _observerDictionary.Count - 1; i >= 0; i--)
            {
                Detach(observer, NameSpaces[i]);
            }
            //Detach(id, Namespaces.DefaultNamespace);
        }

        public void Detach(IEventObserver observer, string fqns)
        {
            if (!_observerDictionary.ContainsKey(fqns)) return;
            _observerDictionary[fqns].Remove(observer);
        }

        public delegate void UserLoggedInEventHandler(Object sender, UserLoggedInEventArgs e);
        public delegate void UserLoggedOutEventHandler(Object sender, UserLoggedOutEventArgs e);
        public delegate void CloseCAALHPEventHandler(Object sender, EventArgs e);

        public void Notify(KeyValuePair<string, string> value)
        {
            var fqns = value.Key;
            if (!_observerDictionary.ContainsKey(fqns)) return;
            for (var i = _observerDictionary[fqns].Count - 1; i >= 0; i--)
            {
                var i1 = i;
                Task.Run(() => _observerDictionary[fqns][i1].Update(value));
            }
            //if the event is a user related event, raise a .net event so we can change state in CAALHP
            HandleUserEvents(fqns);
        }

        private void HandleUserEvents(string fqns)
        {
            var eventType = EventHelper.GetTypeFromFullyQualifiedNameSpace(fqns);
            if (eventType == typeof(UserLoggedInEvent))
            {
                OnUserLoggedIn();
            }
            else if (eventType == typeof(UserLoggedOutEvent))
            {
                OnUserLoggedOut();
            }
            else if (eventType == typeof (GenericInfoEvent))
            {
                //filtering is done in EventSubject.OnCloseCAALHP()
                OnCloseCAALHP();
            }
        }

        protected abstract void OnUserLoggedOut();
        protected abstract void OnUserLoggedIn();
        protected abstract void OnCloseCAALHP();
    }
}
