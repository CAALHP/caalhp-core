﻿namespace CAALHP.Library.Managers
{
    public interface ILifeCycleManager
    {
        void StartPlugins();
        void StopPlugins();
    }
}