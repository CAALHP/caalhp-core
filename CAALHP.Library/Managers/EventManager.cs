﻿using System;
using CAALHP.Library.Observer;

namespace CAALHP.Library.Managers
{
    public class EventManager : IEventManager, IObservable<EventSubject>
    {
        private readonly EventSubject _eventSubject;

        public EventSubject EventSubject
        {
            get { return _eventSubject; }
        }

        public EventManager()
        {
            _eventSubject = new EventSubject();
        }

        public IDisposable Subscribe(IObserver<EventSubject> observer)
        {
            throw new NotImplementedException();
        }
    }
}
