﻿using CAALHP.Library.Observer;

namespace CAALHP.Library.Managers
{
    public interface IEventManager
    {
        EventSubject EventSubject { get; }
    }
}