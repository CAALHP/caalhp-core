﻿using CAALHP.Library.Hosts;

namespace CAALHP.Library.Visitor
{
    public interface IHostVisitor
    {
        void Visit(ICAALHP caalhp);
        void Visit(IHostManager host); 
        void Visit(IAppHost host);
        void Visit(IDeviceDriverHost host);
        void Visit(IServiceHost host);
    }
}