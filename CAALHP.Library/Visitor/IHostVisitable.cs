﻿namespace CAALHP.Library.Visitor
{
    public interface IHostVisitable
    {
        void Accept(IHostVisitor visitor);
    }
}