﻿using System.Collections.Generic;
using CAALHP.Library.Config;
using CAALHP.Library.Hosts;

namespace CAALHP.Library.Visitor
{
    public class GetListOfInstalledAppsUserLoggedInVisitor : IHostVisitor
    {
        public List<PluginConfig> Result { get; private set; }

        public GetListOfInstalledAppsUserLoggedInVisitor()
        {
            Result = new List<PluginConfig>();
        }

        public void Visit(IAppHost host)
        {
            Result.AddRange(host.GetLocalInstalledApps());
            //filter result
        }

        public void Visit(IDeviceDriverHost host) { }
        public void Visit(IServiceHost host) { }
        public void Visit(ICAALHP caalhp) { }
        public void Visit(IHostManager hostManager) { }
    }
}