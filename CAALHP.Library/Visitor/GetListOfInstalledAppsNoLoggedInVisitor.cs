using System.Collections.Generic;
using System.Linq;
using CAALHP.Contracts;
using CAALHP.Library.Config;
using CAALHP.Library.Hosts;

namespace CAALHP.Library.Visitor
{
    public class GetListOfInstalledAppsNotLoggedInVisitor : IHostVisitor
    {
        public List<PluginConfig> Result { get; private set; }

        public GetListOfInstalledAppsNotLoggedInVisitor()
        {
            Result = new List<PluginConfig>();
        }

        public void Visit(IAppHost host)
        {
            Result.AddRange(host.GetLocalInstalledApps().Where(x => !x.Config.RequiresLogin));
            //filter result
        }

        public void Visit(IDeviceDriverHost host) { }
        public void Visit(IServiceHost host) { }
        public void Visit(ICAALHP caalhp) { }
        public void Visit(IHostManager hostManager) { }
    }
}