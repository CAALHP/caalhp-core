﻿using System.Collections.Generic;
using CAALHP.Library.Config;

namespace CAALHP.Library.Brokers
{
    public class CAALHPBroker : ICAALHPBroker
    {
        private readonly ICAALHP _caalhp;

        public CAALHPBroker(ICAALHP caalhp)
        {
            _caalhp = caalhp;
        }

        public void ShowApp(string appName)
        {
            _caalhp.StartApp(appName);
        }

        public void CloseApp(string appName)
        {
            _caalhp.CloseApp(appName);
        }

        public IList<PluginConfig> GetListOfInstalledApps()
        {
            return _caalhp.GetListOfInstalledApps();
        }

        public IList<PluginConfig> GetListOfInstalledDeviceDrivers()
        {
            return _caalhp.GetListOfInstalledDeviceDrivers();
        }

        public void ActivateDeviceDrivers()
        {
            _caalhp.ActivateDeviceDrivers();
        }

        public IList<PluginConfig> GetListOfRunningApps()
        {
            return _caalhp.GetListOfRunningApps();
        }
    }
}