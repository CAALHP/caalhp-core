using System.Collections.Generic;
using CAALHP.Library.Config;
using CAALHP.Library.Hosts;
using CAALHP.Library.Managers;
using CAALHP.Library.State;
using CAALHP.Library.Visitor;

namespace CAALHP.Library
{
    public interface ICAALHP : IHostVisitable
    {
        void StartApp(string appName);
        void CloseApp(string appName);
        List<PluginConfig> GetListOfInstalledApps();
        IList<PluginConfig> GetListOfInstalledDeviceDrivers();
        void ActivateDeviceDrivers();
        IList<IHostManager> HostManagers { get; }
        EventManager EventManager { get; }
        IUserState UserState { get; set; }
        IList<PluginConfig> GetListOfRunningApps();
    }
}