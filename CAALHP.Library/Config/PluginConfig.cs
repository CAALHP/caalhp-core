﻿namespace CAALHP.Library.Config
{
    public class PluginConfig
    {
        public CareStorePluginConfig Config { get; set; }
        public string Directory { get; set; }
    }
}
