﻿namespace CAALHP.Events
{
    public class SimpleMeasurementEvent : Event
    {
        public string MeasurementType { get; set; }
        public double Value { get; set; }
        public string UserId { get; set; }
    }
}
