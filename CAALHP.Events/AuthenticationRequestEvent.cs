﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;

namespace CAALHP.Events
{
    public class AuthenticationRequestEvent : Event
    {
        public string Username { get; set; }
        public string Pincode { get; set; }
        public RequestTypes ReqType { get; set; }
    }
}
