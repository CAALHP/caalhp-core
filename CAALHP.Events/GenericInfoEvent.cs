﻿namespace CAALHP.Events
{
    public class GenericInfoEvent : Event
    {
        public string Info { get; set; }
    }
}