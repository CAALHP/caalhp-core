﻿using System;

namespace CAALHP.Events
{
    public class DeviceProfileEvent : Event
    {
        public Guid DeviceId { get; set; }

        public string ModelName { get; set; }

        public string DeviceType { get; set; }

        public string ManufacturerName { get; set; }
    }
}
