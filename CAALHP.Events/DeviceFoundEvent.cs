﻿namespace CAALHP.Events
{
    public class DeviceFoundEvent : Event
    {
        public string Name { get; set; }
        public string DeviceDriverFileName { get; set; }
    }
}
