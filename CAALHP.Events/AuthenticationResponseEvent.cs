﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;

namespace CAALHP.Events
{
    public class AuthenticationResponseEvent
    {
        public string Username { get; set; }
        public Boolean Response {get; set;}
        public RequestTypes ReqType { get; set; }
        public string Message { get; set; }

    }
}
