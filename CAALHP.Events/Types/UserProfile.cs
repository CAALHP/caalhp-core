﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAALHP.Events.Types
{
    public class UserProfile
    {
        public UserProfile()
        {
            CreationTime = DateTime.Now.ToUniversalTime().ToString(CultureInfo.InvariantCulture);
            UserId = "";
            UserName = "";
            FullName = "";
            Height = "";
            DateOfBirth = "";
            UserLevel = CareStoreUserLevel.Citizen;
            Email = "";
            MobileNumber = "";
            Pin = "";

            //PhotoLink = "";
        }

        //TODO consider change ot constructor initialization.

        public string CreationTime { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Height { get; set; }
        public string DateOfBirth { get; set; }
        public CareStoreUserLevel UserLevel { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public string Pin { get; set; }
        public bool FingerPrint { get; set; }
        public bool FacialProfile { get; set; }
        public bool HasPhoto { get; set; }
        public string PhotoLink
        {
            get { return UserId + ".jpg"; }
        }
    }
}
