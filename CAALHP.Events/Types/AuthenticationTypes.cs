﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAALHP.Events.Types
{
    public enum RequestTypes
    {
        AuthByFinger,
        AuthByPincode,
        Enroll,
        CheckforScanner,
        DeleteUser,
        ClearDb
    }
}
