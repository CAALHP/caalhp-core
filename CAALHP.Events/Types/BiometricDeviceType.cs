﻿namespace CAALHP.Events.Types
{
    public enum BiometricDeviceType
    {
        Facial,
        Finger,
        Voice
    }
}