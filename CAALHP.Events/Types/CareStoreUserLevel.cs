﻿namespace CAALHP.Events.Types
{
    public enum CareStoreUserLevel
    {
        Caretaker,
        Citizen
    }
}