﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;

namespace CAALHP.Events.UserServiceEvents
{
    public class CreateUserLocalEvent: Event
    {
        public UserProfile UserProfile { get; set; }
        public string PhotoRoot { get; set; }
        public bool NewPhoto { get; set; }
    }
}
