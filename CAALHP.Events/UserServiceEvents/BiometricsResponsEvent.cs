﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;

namespace CAALHP.Events.UserServiceEvents
{
    public class BiometricsResponsEvent : Event
    {
        public BiometricDeviceType Type { get; set; }
    }
}
