﻿namespace CAALHP.Events
{
    public class DownloadDeviceDriverEvent : Event
    {
        public string DeviceDriverFileName { get; set; }
    }
}
