﻿namespace CAALHP.Events
{
    public class ErrorEvent : Event
    {
        public string ErrorMessage { get; set; }
    }
}
