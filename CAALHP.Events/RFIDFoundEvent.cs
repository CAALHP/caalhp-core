﻿namespace CAALHP.Events
{
    public class RFIDFoundEvent : Event
    {
        public string Tag { get; set; }
    }
}
