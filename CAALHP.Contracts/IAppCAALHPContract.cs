﻿using System.Collections.Generic;

namespace CAALHP.Contracts
{
    public interface IAppCAALHPContract : IBaseCAALHPContract
    {
        void Initialize(IAppHostCAALHPContract hostObj, int processId); 
        void Show();
    }
}
