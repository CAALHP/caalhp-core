﻿using System.Collections.Generic;

namespace CAALHP.Contracts
{
    public interface IAppHostCAALHPContract
    {
        IHostCAALHPContract Host { get; set; }
        void ShowApp(string appName);
        void CloseApp(string appName);
        IList<IPluginInfo> GetListOfInstalledApps();
    }
}