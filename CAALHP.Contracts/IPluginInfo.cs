﻿namespace CAALHP.Contracts
{
    public interface IPluginInfo
    {
        string Name { get; set; }
        string LocationDir { get; set; }
    }
}