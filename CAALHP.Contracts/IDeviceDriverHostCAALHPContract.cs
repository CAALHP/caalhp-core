﻿namespace CAALHP.Contracts
{
    public interface IDeviceDriverHostCAALHPContract
    {
        IHostCAALHPContract Host { get; set; }
    }
}