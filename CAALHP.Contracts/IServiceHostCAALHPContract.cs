﻿using System.Collections.Generic;

namespace CAALHP.Contracts
{
    public interface IServiceHostCAALHPContract
    {
        IHostCAALHPContract Host { get; set; }
        IList<IPluginInfo> GetListOfInstalledApps();
        IList<IPluginInfo> GetListOfInstalledDeviceDrivers();
        void CloseApp(string fileName);
        void ActivateDeviceDrivers();
        IList<string> GetListOfEventTypes();
    }
}