﻿namespace CAALHP.Contracts
{
    public interface IServiceCAALHPContract : IBaseCAALHPContract
    {
        void Initialize(IServiceHostCAALHPContract hostObj, int processId); 
        void Start();
        void Stop();
    }
}
