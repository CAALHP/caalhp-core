﻿using System.Collections.Generic;

namespace CAALHP.Contracts
{
    public interface IBaseCAALHPContract
    {
        void Notify(KeyValuePair<string, string> notification);
        string GetName();
        bool IsAlive();
        void ShutDown();
    }
}