﻿using System;
using System.Runtime.Serialization;

namespace CareStoreServiceContracts
{

    [Serializable]
    [DataContract]
    public class UserProfileDataObject
    {

        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string CreationTime { get; set; }

        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Pin { get; set; }

        [DataMember]
        public string UserLevel { get; set; }

        [DataMember]
        public string DateOfBirth { get; set; }
        
        [DataMember]
        public string MobileNumber { get; set; }

        [DataMember]
        public string Height { get; set; }

        [DataMember]
        public bool HasProfileImage { get; set; }

        [DataMember]
        public bool HasFingerPrintTemplate { get; set; }

        [DataMember]
        public bool HasFacialProfileTemplate { get; set; }
    }
}

