﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace CareStoreServiceContracts
{
    [ServiceContract]
    public interface ICareStoreMarketplaceContract
    {
        [OperationContract]
        Uri GetDriverLink(string profile);

        [OperationContract]
        IList<string> GetAppLinks();
        
        #region UserImplementation

        //[OperationContract]
        //void UpdateUser(UserProfileDataObject profile);

        //[OperationContract]
        //void UpdateUser(UserProfileDataObject profile, byte[] profilePicture);

        //[OperationContract]
        //void UpdateUser(UserProfileDataObject profile, byte[] profilePicture, byte[] facialTemplate);

        [OperationContract]
        void UpdateUser(UserProfileDataObject profile, byte[] profilePicture, byte[] facialTemplate,
                        byte[] fingerTemplate);

        [OperationContract]
        void UpdateFacialTemplate(string id, byte[] template);

        [OperationContract]
        void UpdateFingerTemplate(string id, byte[] template);

        [OperationContract]
        void UpdateProfilePicture(string id, byte[] template);

        [OperationContract]
        List<UserProfileDataObject> GetUsers();

        [OperationContract]
        byte[] GetProfilePicture(string id);

        [OperationContract]
        byte[] GetFacialTemplate(string id);

        [OperationContract]
        byte[] GetFingerTemplate(string id);

        [OperationContract]
        void RemoveUser(string id);

        #endregion


        /*
        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);
         */
    }
}