﻿using System;
using System.Runtime.Serialization;

namespace CareStoreServiceContracts
{
    [DataContract]
    public class DeviceProfile
    {
        [DataMember]
        public Guid DeviceId { get; set; }

        [DataMember]
        public string ModelName { get; set; }

        [DataMember]
        public string DeviceType { get; set; }

        [DataMember]
        public string ManufacturerName { get; set; }
    }
}
