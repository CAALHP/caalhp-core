﻿using System;
using System.Runtime.Serialization;

namespace CareStoreServiceContracts
{
    [DataContract]
    public class SecurityToken
    {
        [DataMember]
        public Guid UserId { get; set; }

        [DataMember]
        public bool AuthenticationDecision { get; set; }
    }
}