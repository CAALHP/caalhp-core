﻿using System.ServiceModel;

namespace CareStoreServiceContracts
{
    [ServiceContract]
    public interface ICAALHPServiceContract
    {
        [OperationContract]
        void AddDevice(DeviceProfile profile);
    }
}
