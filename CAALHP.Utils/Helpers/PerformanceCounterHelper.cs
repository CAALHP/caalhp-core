﻿using System;
using System.Diagnostics;

namespace CAALHP.Utils.Helpers
{
    public static class PerformanceCounterHelper
    {
        public static PerformanceCounter GetPerformanceCounterByProcessId(int processId)
        {
            var cat = new PerformanceCounterCategory("Process");
            var instanceNames = cat.GetInstanceNames();
            foreach (var name in instanceNames)
            {
                using (var cnt = new PerformanceCounter("Process", "ID Process", name, true))
                {
                    try
                    {
                        var val = (int)cnt.RawValue;
                        if (val == processId)
                        {
                            return cnt;
                        }
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                    
                }
            }
            return null;
        }
    }
}
